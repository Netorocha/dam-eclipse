package com.netoav1.idez.model;

import javax.ejb.Local;

import com.netoav1.idez.model.Issue;

@Local
public interface BugTracker {

	public void addIssue( Issue issue );
	
}
