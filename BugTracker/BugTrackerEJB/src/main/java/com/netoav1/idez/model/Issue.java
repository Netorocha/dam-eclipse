package com.netoav1.idez.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Email;

public class Issue implements Serializable {

	private static final long serialVersionUID = 1L;

	private String description;
	
	@Email
	private String reporter;
	
	@Past
	private Date discoveredDate;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public Date getDiscoveredDate() {
		return discoveredDate;
	}

	public void setDiscoveredDate(Date discoveredDate) {
		this.discoveredDate = discoveredDate;
	}
	
	
	
}
