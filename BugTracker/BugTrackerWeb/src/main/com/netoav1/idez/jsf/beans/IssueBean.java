package main.com.netoav1.idez.jsf.beans;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.netoav1.idez.model.BugTracker;
import com.netoav1.idez.model.Issue;

@Named
@RequestScoped
public class IssueBean {

	private Issue issue;
	
	@EJB
	private BugTracker tracker;
	
	public IssueBean() {
		this.issue = new Issue();
	}
	
	public Issue getIssue() {
		return issue;
	}

	public void setIssue(Issue issue) {
		this.issue = issue;
	}

	public String save(){
		tracker.addIssue(this.issue);
		return "sucesso";
	}
	
	
}
